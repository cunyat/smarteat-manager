'use strict';


angular

  .module('booking', ['ionic'])

  .config(configureBookingStates);


//////////////////////////////////////////////////////////////////

/* @ngInject */
function configureBookingStates ($stateProvider) {
  $stateProvider
    .state('main.booking', {
      url: '/booking',
      views: {
        'content': {
          templateUrl: 'booking/templates/booking.html',
          controller: 'bookingCtrl as vm'
        }
      }
    })
    .state('main.validation', {
      url: '/validation',
      views: {
        'content': {
          templateUrl: 'booking/templates/validation.html',
          controller: 'validationCtrl as vm'
        }
      }
    });
}
