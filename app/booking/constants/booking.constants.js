'use strict';
angular.module('booking')
.constant('BookingConfig', {

  // gulp environment: injects environment vars
  ENV: {
    /*inject-env*/
    /*endinject*/
  },

  // gulp build-vars: injects build vars
  BUILD: {
    /*inject-build*/
    /*endinject*/
  }

});
