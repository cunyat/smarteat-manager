'use strict';

angular

  .module('main')

  .controller('bookingCtrl', bookingController);

//

/* @ngInject */
function bookingController () {
  var vm = this;

  vm.bookings = [];

  activate();

  ////////////////////////////////////////

  function activate () {
    vm.bookings = loadBookings();
  }

  function loadBookings () {
    return [
      {
        name: 'Ramon Cunyat Llanes',
        commensals: 4,
        date: '04/02/2016',
        phone: '652 17 45 12',
        time: '13:45'
      }, {
        name: 'Joan Marc Huget Sole',
        commensals: 3,
        date: '04/02/2016',
        phone: '657 42 68 41',
        time: '14:30'
      }, {
        name: 'Joan Marc Huget Sole',
        commensals: 3,
        date: '04/02/2016',
        phone: '657 42 68 41',
        time: '14:30'
      }, {
        name: 'Joan Marc Huget Sole',
        commensals: 3,
        date: '04/02/2016',
        phone: '657 42 68 41',
        time: '14:30'
      }, {
        name: 'Joan Marc Huget Sole',
        commensals: 3,
        date: '04/02/2016',
        phone: '657 42 68 41',
        time: '14:30'
      }];
  }
}
