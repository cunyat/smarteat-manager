'use strict';

angular

  .module('booking')

  .controller('validationCtrl', validationController);


//////////////////////////////////////////////////////////////


/* @ngInject */
function validationController (booking) {
  /* jshint validthis:true */
  var vm = this;

  vm.bookings = [];

  vm.swipeAccept = swipeAccept;
  vm.swipeReject = swipeReject;

  vm.accept = accept;
  vm.reject = reject;

  activate();

  /////////////////////////////////


  function activate () {
    booking.validations().then(function (data) {
      vm.bookings = data;
    });
  }

  function swipeAccept () {
    // TODO: implement accepts/rejects
    console.log('Swipe accept');
    accept();
  }
  function swipeReject () {
    console.log('Swipe reject');
    reject();
  }
  function accept () {
    console.log('accept');
  }
  function reject () {
    console.log('reject');
  }
}
