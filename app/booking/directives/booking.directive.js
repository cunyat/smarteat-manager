'use strict';


angular

  .module('main')


  .directive('booking', bookingDirective)

;


/* @ngInject */
function bookingDirective () {
  return {
    restrict: 'E',
    templateUrl: 'booking/templates/booking.directive.html',
    scope: {
      model: '=',
      buttons: '@'
    },
//    link: bookingLink
  };
}
