'use strict';
angular

  .module('data')


  .factory('booking', bookingService);


/* @ngInject */
function bookingService ($q) {

  var s = {
    validationsCount: 4,
    validations: requestValidations
  };

  return s;

  //////////////////////////////////////////////////////////


  function requestValidations () {
    var validations =
    [{
      name: 'Ramon Cunyat Llanes',
      commensals: 4,
      date: '04/02/2016',
      //phone: '652 17 45 12',
      time: '13:45'
    }, {
      name: 'Joan Marc Huget Sole',
      commensals: 3,
      date: '04/02/2016',
      //phone: '657 42 68 41',
      time: '14:30'
    }];

    return $q(function (resolve) { resolve(validations); });
  }

}
