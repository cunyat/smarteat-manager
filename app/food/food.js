'use strict';


angular

  .module('food', ['ionic'])


  .config(configureFoodStates);


///////////////////////////////////////////////////////////////////////////////
/* @ngInject */
function configureFoodStates ($stateProvider) {
  $stateProvider
    .state('main.menus', {
      url: '/menus',
      views: {
        'content': {
          templateUrl: 'food/templates/menus.html',
          controller: 'menusCtrl as vm'
        }
      }
    })
    .state('main.carte', {
      url: '/carte',
      views: {
        'content': {
          templateUrl: 'food/templates/carte.html',
          controller: 'carteCtrl as vm'
        }
      }
    });
}
