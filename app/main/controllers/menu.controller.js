'use strict';


angular

  .module('main')

  .controller('menuCtrl', menuController)

;


/* @ngInject */
function menuController ($timeout, booking) {
  /* jshint validthis:true */
  var vm = this;

  // Bookings pendent to validate
  vm.vcount = 0;


  activate();

  ////////////////////////////

  function activate () {

    // TODO: Remove todo when server call is implemented
    $timeout(function () {
      vm.vcount = booking.validationsCount;
    }, 5 * 1000);
  }
}
