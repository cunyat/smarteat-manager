'use strict';


angular

  .module('main', ['ionic', 'ngCordova', 'booking', 'data', 'food', 'place'])


  .config(configureMainStates)

  .run(configureIonic);


////////////////////////////////////////////////////////////

/* @ngInject */
function configureMainStates ($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('/main/booking');

  $stateProvider
    .state('main', {
      url: '/main',
      abstract: true,
      templateUrl: 'main/templates/menu.html',
      controller: 'menuCtrl as vm'
    });
}

/* @ngInject */
function configureIonic ($ionicPlatform, $cordovaStatusbar, $cordovaKeyboard) {
  $ionicPlatform.ready(function () {
    if (!window.cordova) { return; }
    if ($cordovaKeyboard) {
      $cordovaKeyboard.hideAccessoryBar(true);
    }
    if ($cordovaStatusbar) {
      $cordovaStatusbar.styleDefault();
    }
  });
}
