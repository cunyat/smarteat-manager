'use strict';


angular

  .module('place', ['ionic'])


  .config(configurePlaceStates);


////////////////////////////////////////////////////////////////////////////////

/* @ngInject */
function configurePlaceStates ($stateProvider) {
  $stateProvider
    .state('main.info', {
      url: '/info',
      views: {
        'content': {
          templateUrl: 'place/templates/info.html',
          controller: 'infoCtrl as vm'
        }
      }
    })
    .state('main.hours', {
      url: '/hours',
      views: {
        'content': {
          templateUrl: 'place/templates/hours.html',
          controller: 'hoursCtrl as vm'
        }
      }
    })
    .state('main.tables', {
      url: '/tables',
      views: {
        'content': {
          templateUrl: 'place/templates/tables.html',
          controller: 'tablesCtrl as vm'
        }
      }
    });
}
