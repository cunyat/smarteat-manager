'use strict';

describe('module: data, service: Booking', function () {

  // load the service's module
  beforeEach(module('data'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var Booking;
  beforeEach(inject(function (_Booking_) {
    Booking = _Booking_;
  }));

  it('should do something', function () {
    expect(!!Booking).toBe(true);
  });

});
