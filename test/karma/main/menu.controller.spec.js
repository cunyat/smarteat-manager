'use strict';

describe('module: main, controller: MenuCtrl', function () {

  // load the controller's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate controller
  var menuCtrl;
  beforeEach(inject(function ($controller) {
    menuCtrl = $controller('MenuCtrl');
  }));

  it('should do something', function () {
    expect(!!menuCtrl).toBe(true);
  });

});
